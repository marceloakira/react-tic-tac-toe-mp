const close = (client) => {
  return () => {
    client.socket && client.socket.close()
  }
}
const init = (client) => {
  return (socket, endpoint, username) => {
    client.socket = socket
    client.endpoint = endpoint
    client.connected = true
    client.username = username
  }
}
const reset = (client) => {
  return () => {
    client.socket = null
    client.connected = false
    client.endpoint = null  
  }
}
const send = (client) => {
  return (message) => {
    return new Promise(
      (resolve, reject) => {
        try {
          client.socket.send(message)
          resolve(message)
        }
        catch(error) {
          reject(error)
        }
      }
    )  
  }
}

module.exports = {
  close: close,
  init: init,
  reset: reset,
  send: send,
}