import React from 'react'
import {Grid, GridList} from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import MessageContainer from './MessageContainer'

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  gridList: {
    width: 300,
    height: 300,
    outline: '1px solid lightgrey'    
  }
}));

export default function MessagePanel(props) {
  const messages = props.messages? props.messages: []
  const classes = useStyles();
  const renderMessageContainer = (message) =>
    <Grid item xs={12}>
      <MessageContainer
        message={message}
      />
    </Grid> 
  return (
    <div className={classes.root}>
      <GridList
        className={classes.gridList}
        cols={1} 
        cellHeight='auto' 
        spacing={10}
      >
        {
          messages.map(renderMessageContainer) 
        }
      </GridList>
    </div>
  )
}