export const messages = [
  {
    align: 'left',
    author: 'Alice',
    text: 'Mauris in massa in nibh condimentum dictum.'
  },
  {
    align: 'right',
    author: 'Bob',
    text: ' Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
  },
  {
    align: 'left',
    author: 'Alice',
    text: 'Maecenas in elit quis sem tempor volutpat eget eu felis.'
  },
  {
    align: 'right',
    author: 'Bob',
    text: ' Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
  },  
]