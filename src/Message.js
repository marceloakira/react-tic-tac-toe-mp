import React from 'react'
import { Paper, Typography } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles(theme => ({
  paperLeft: {
    padding: theme.spacing(2),
    textAlign: 'left',
    color: theme.palette.text.secondary,
  },
  paperRight: {
    padding: theme.spacing(2),
    textAlign: 'right',
    color: theme.palette.text.secondary,
    background: '#E5FFCC'    
  }
}));

export default function Message(props) {
  const classes = useStyles();
  const align = props.align === 'right'? classes.paperRight : classes.paperLeft
  return (
    <Paper className={align}>
      <Typography variant="caption">{props.author}</Typography>
      <Typography variant="body2">{props.text}</Typography>
    </Paper>
  )
}