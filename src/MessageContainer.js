import React from 'react'
import { Grid } from '@material-ui/core'
import Message from './Message'

export default function MessageContainer(props) {
  const {
    align,
    author,
    text
  } = props.message

  return (
    <Grid
      container
      spacing={3} 
      xs={12} 
      justify={ align === 'left'? 'flex-start':'flex-end'}
    >
      <Grid item xs={8}>
        <Message align={align} author={author} text={text} />
      </Grid>
    </Grid>
  )
}

