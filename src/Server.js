const port = 8000;
const WebSocket = require('ws')
const wss = new WebSocket.Server({port})

wss.on('connection', (ws, req) => {
  const ip = req.connection.remoteAddress
  const port = req.connection.remotePort
  console.log('client connected: '+ip+':'+port)
  ws.on('message', (msg) => {
    console.log('Message received: ',JSON.stringify(msg))
    wss.clients.forEach((client) => {
      if ( client !== ws && client.readyState === WebSocket.OPEN)
        client.send(msg)
    })
  })
  ws.on('close', () => {
    console.log('client disconnected: '+ip+':'+port)
  })
})
console.log('listening on port', port)