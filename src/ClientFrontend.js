const ClientCommon = require('./ClientCommon')
const send = ClientCommon.send
const reset = ClientCommon.reset
const init = ClientCommon.init
const close = ClientCommon.close

const Client = {
  socket: null,
  endpoint: null,
  connected: false,
  onMessage: [],
  onOpen: [],
  onClose: [],
  username: '',
  connect: (endpoint, username) => {
    if ( Client.socket && ! endpoint )
      return new Promise( 
        (resolve, reject) => {
          resolve(Client.socket)
        }
      )
    else {
      return new Promise(
        (resolve, reject) => {
          var ws = new WebSocket(endpoint)
          ws.onopen = () => { resolve(ws) }
          ws.onerror = (error) => { reject(error) }
      }).then(
        (socket) => {
          Client.init(socket, endpoint, username)
          Client.onOpen && Client.onOpen.forEach(
            (handleOpen) => {
              handleOpen({socket, username, endpoint})
            }
          )
          socket.onclose = () => { 
            Client.reset()
            Client.onClose && Client.onClose.forEach((handleClose)=>
              handleClose()
            )
          }
          socket.onmessage = (evt) => {
            Client.onMessage && Client.onMessage.forEach(
              (handleMessage) => {
                handleMessage(evt.data)
              }
            )
          }
          return socket
        }
      )
    }  
  }
}

Object.assign(Client,{close: close(Client)})
Object.assign(Client,{init: init(Client)})
Object.assign(Client,{reset: reset(Client)})
Object.assign(Client,{send: send(Client)})

export default Client