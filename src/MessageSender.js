import React, {useState, useEffect} from 'react'
import { TextField, Grid, Fab } from '@material-ui/core'
import { FiSend } from "react-icons/fi"; 
import ConnectButton from './ConnectButton'
import Client from './ClientFrontend'

export default function MessageSender(props) {
  const [ connected, setConnected ] = useState(false)
  const [ text, setText ] = useState('')
  const [ username, setUsername ] = useState('')
  const handleSend = () => {
    Client.connect().then(
      () => {        
        Client.send(JSON.stringify({username, text})) 
      }).catch(
        (error) => { console.log('message not sent: '+error)}
      )
    props.onLocalMessage(JSON.stringify({username,text}))
    setText('')
  }
  const handleChange = (event) => {
    setText(event.target.value)
  }
  const handleOpen = (payload) => {
    setConnected(true)
    setUsername(payload.username)
  }
  const handleClose = () => {
    setConnected(false)
    setUsername('')
  }
  useEffect(() => {
    Client.onOpen.push(handleOpen)
    Client.onClose.push(handleClose)
    return () => {
      Client.close()
    }
  },[])
  return(
    <div>
      <Grid container direction="row" justify="flex-start">
        <Grid item>
          <TextField
            onChange={handleChange}
            multiline
            rows={4}
            variant="outlined"
            placeholder={
              connected? "Type a message": "Connect to send a message"
            }
            value={text}
            disabled={connected? false: true}
          />
        </Grid>
        <Grid item>
          <Grid 
            container 
            direction="column" 
            alignItems="flex-start"
            spacing={1}
          >
            <Grid item>
              <ConnectButton/>
            </Grid>
            <Grid item>
              <Fab
                disable={connected? 'true': 'false'} 
                onClick={handleSend}
                color={ connected? "primary": "default"}>
                <FiSend />
              </Fab>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </div>
  )
}