import React, {useState, useEffect} from 'react'
import { Dialog, DialogContent, DialogActions, DialogTitle, 
  Button, TextField } from '@material-ui/core'

export default function ConnectDialog(props) {
  const [ open, setOpen ] = useState(props.open)
  const [ hostname, setHostname ] = useState('')
  const [ username, setUsername ] = useState('')
  const { connected, title, onConnect} = props
  useEffect(() => {
    setOpen(props.open)
  }, [props.open])
  const handleClose = () => {setOpen(false)}
  return(
    <Dialog open={open}>
      <DialogTitle>{title}</DialogTitle>
      {
        ! connected &&
        <DialogContent>
          <TextField label='Hostname' value={hostname}
            onInput={e=>setHostname(e.target.value)}/>
          <TextField label='Username' value={username}
            onInput={e=>setUsername(e.target.value)}/>
        </DialogContent>
      }
      <DialogActions>
        <Button onClick={handleClose} color="primary">
          Cancel
        </Button>
        <Button color="primary" 
          onClick={
            () => onConnect({hostname,username}) 
          }>
          { ! connected ? "Connect": "Disconnect" }
        </Button>
      </DialogActions>
    </Dialog>
  )
}