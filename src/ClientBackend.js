const WebSocket = require('ws')
const ClientCommon = require('./ClientCommon')
const send = ClientCommon.send
const reset = ClientCommon.reset
const init = ClientCommon.init

const Client = {
  socket: null,
  endpoint: null,
  connected: false,
  onMessage: null,
  connect: (endpoint) => {
    if ( Client.socket && ! endpoint )
      return new Promise( 
        (resolve, reject) => {
          resolve(Client.socket)
        }
      )
    else {
      return new Promise(
        (resolve, reject) => {
          var ws = new WebSocket(endpoint)
          ws.on('open', () => { resolve(ws) })
          ws.on('error', (error) => { reject(error) })
      }).then(
        (socket) => {
          Client.init(socket, endpoint)
          socket.on('close', () => { Client.reset() })
          socket.on('message', (msg) => { 
            Client.onMessage && Client.onMessage(msg)
          })
          return socket
        }
      )
    }  
  }
}

Object.assign(Client,{send: send(Client)})
Object.assign(Client,{reset: reset(Client)})
Object.assign(Client,{init: init(Client)})

module.exports = {
  Client: Client
}