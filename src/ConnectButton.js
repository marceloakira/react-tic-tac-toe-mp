import React, {useState, useEffect} from 'react'
import ConnectionStatus from './ConnectionStatus'
import ConnectDialog from './ConnectDialog'
import Client from './ClientFrontend'

export default function ConnectButton(props) {
  const [connected, setConnected] = useState(false)
  const [open, setOpen] = useState(false)
  const tipDisconnected = 'Connect to a WebSocket Server (e.g. localhost:8000)'
  const [tooltip, setTooltip] = useState(tipDisconnected)
  const toggleDialog = () => { open? setOpen(false): setOpen(true)}
  const disconnect = () => {
    Client.close()
    setConnected(false)
    setTooltip(tipDisconnected)    
  }
  const connect = (payload) => {
    const {hostname, username} = payload
    const endpoint = 'ws://'+hostname
    Client.connect(endpoint, username)
      .then((socket)=>{ 
        setConnected(true)
        setTooltip("Connected at "+endpoint+" as "+username)    
        Client.onClose.push(() => disconnect())
      })
  }
  const toggleConnect = (payload) => {
    connected? disconnect(): connect(payload)
    toggleDialog()
  }
  useEffect(()=>{
    connected && Client.close()
  },[])
  return (
    <>
      <ConnectionStatus
        onClick={toggleDialog}
        tooltip={tooltip}
        connected={connected}
      />
      <ConnectDialog
        connected={connected}
        title={tooltip}
        onConnect={toggleConnect}
        open={open}
      />
    </>
  )
}