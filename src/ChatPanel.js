import React, {useState, useEffect} from 'react'
import { Grid } from '@material-ui/core'
import MessagePanel from './MessagePanel'
import MessageSender from './MessageSender'
import Client from './ClientFrontend'

export default function ChatPanel(props) {
  const [ messages, setMessages ] = useState([])
  const [ connected, setConnected ] = useState(false)
  const [ username, setUsername ] = useState('Anonymous')
  const handleOpen = (payload) => {
    setConnected(true)
    setUsername(payload.username)
  }
  const handleClose = () => {
    setConnected(false)
    setUsername('Anonymous')
  }  
  const handleMessage = (payload) => {
    const msg_received = JSON.parse(payload)    
    let message = {}
    if ( msg_received.username === username ) 
      message.align = 'right'
    else
      message.align = 'left'
    message.author = msg_received.username
    message.text = msg_received.text
    setMessages(msgs => msgs.concat(message))
  }  
  useEffect(()=> {
    Client.onOpen.push(handleOpen)
    Client.onClose.push(handleClose)
    Client.onMessage.push(handleMessage)    
  },[])
  return(
    <Grid 
      container
      direction="column" 
      alignItems="flex-start"
      spacing={2}
    >
      <Grid item>
        <MessagePanel
          messages={messages}
        />
      </Grid>
      <Grid item>
        <MessageSender
          onLocalMessage={handleMessage}
        />
      </Grid>
    </Grid>
  )
}