import React from 'react'
import { 
  Tooltip,
  Fab
} from '@material-ui/core'
import { FiLogIn, FiLogOut } from "react-icons/fi"; 

export default function ConnectButton(props) {
  return (
    <>
      <Tooltip title={props.tooltip}>
        <Fab
          onClick={props.onClick} 
          // variant="outlined" 
          color={
            ! props.connected ? 'secondary': 'default'
          }
        >
          { ! props.connected ? <FiLogIn />: <FiLogOut />}
        </Fab>
      </Tooltip>
    </>
  )
}