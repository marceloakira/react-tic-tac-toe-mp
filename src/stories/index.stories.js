import React from 'react';
import { storiesOf } from '@storybook/react'
import Square from '../Square'
import Board from '../Board'
import MessagePanel from '../MessagePanel'
import Message from '../Message'
import MessageContainer from '../MessageContainer'
import { messages } from '../data'
import ConnectButton from '../ConnectButton';
import MessageSender from '../MessageSender'
import ChatPanel from '../ChatPanel'
import ConnectionStatus from '../ConnectionStatus'
import ConnectDialog from '../ConnectDialog'

storiesOf('tictactoe game', module)
  .add('square', () =>  
    <Square
      width='50'
      height='50'
      value='o'
    />
  )
  .add('board', () =>  
    <Board      
      squares={
        [null, null, null, 
          null, null, null, 
        null, null, null]} 
    />
  )
storiesOf('chat', module)  
  .add('message', () =>
    <Message
      author='Alice'
      message='Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris in massa in nibh condimentum dictum.'
      align='right'
    />
  )  
  .add('message container', () =>
    <MessageContainer
      message={
        messages[1]
      }
    />
  )  
  .add('message panel', () =>
    <MessagePanel
      messages={messages}
    />
  )
  .add('message sender', () => 
    <MessageSender />
  )  
  .add('chat panel', () => 
    <ChatPanel />
  )
storiesOf('connection', module)  
  .add('connnection status disconnected', () =>
    <ConnectionStatus 
      onClick={ () => { alert('clicked')} }
      tooltip='Connect to a WebSocket Server (e.g. localhost:8000)'
      connected={false}
    />
  )
  .add('connnection status connected', () =>
    <ConnectionStatus
      onClick={ () => { alert('clicked')} }
      tooltip='Connected to http://localhost:8000'
      connected={true}
    />
  )
  .add('connnection dialog', () =>
    <ConnectDialog
      connected={false}
      title='Connect to a WebSocket Server (e.g. localhost:8000)'
      onConnect={(payload)=>{ 
          alert('connect clicked with values: '+
          JSON.stringify(payload))
        }
      }
      open={true}
    />
  )
  .add('connect button', () => 
    <ConnectButton />
  )
